const express = require("express");
const md5 = require("crypto-js/md5");
const { MongoClient } = require("mongodb");
const { body, validationResult } = require('express-validator');

const app = express();
app.set("title", "reverse_md5");
const port = process.env.PORT || 4000;

const mongoURI = process.env.MONGO_URL || "mongodb://localhost:27017";
const dbName = "Cluster0";

let client;

async function connectToMongo() {
  try {
    if (!client) {
      client = new MongoClient(mongoURI);
      await client.connect();
    }
  } catch (error) {
    console.error("Error connecting to MongoDB:", error);
    throw error;
  }
}

async function saveMD5(md5Hash, text) {
  let dbo;
  try {
    await connectToMongo();
    dbo = client.db(dbName);
    const myObj = { _id: text, md5: md5Hash };
    await dbo.collection("reverse_md5").insertOne(myObj);
  } catch (error) {
    if (error.code === 11000) { // MongoDB duplicate key error code
      console.warn("Duplicate key error:", error.message);
    } else {
      console.error("Error saving MD5:", error);
      throw error;
    }
  }
}


async function getMD5(md5Hash) {
  try {
    await connectToMongo();
    const dbo = client.db(dbName);
    const myObj = { md5: md5Hash };
    const result = await dbo.collection("reverse_md5").find(myObj).toArray();
    return result;
  } catch (error) {
    console.error("Error getting MD5:", error);
    throw error;
  }
}

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/fr0stb1rd.html");
});

app.get("/md5", [
  body('password').optional().isString().notEmpty(),
  body('md5').optional().isString().notEmpty()
], async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { password, md5: md5Hash } = req.query;

    if ((!password && !md5Hash) || (password && md5Hash)) {
      return res.json({ error: "Usage: https://fr0stb1rd.gitlab.io/posts/Reverse-Hash-API/" });
    }

    if (password) {
      const generatedMD5 = md5(password).toString();
      await saveMD5(generatedMD5, password);
      return res.json({ md5: generatedMD5 });
    } else if (md5Hash) {
      const result = await getMD5(md5Hash);
      return res.json(result);
    }
  } catch (error) {
    console.error("Error in /md5 route:", error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
});

app.listen(port, () => {
  console.log(`reverse_hash app listening on port ${port}`);
});

process.on("uncaughtException", (err) => {
  console.error("Uncaught exception:", err.message, err.name);
});

